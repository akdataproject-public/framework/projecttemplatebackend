# SparkThriftServer

Spark Thrift Server

Framework object (reference): **sths**

HowTo: See the demo


## 1) exposePandasDf(pandasDf, name, cache=True)

Expose a Pandas dataframe to the Spark Thrift database

## 2) exposeSparkDf(sparkDf, name, cache=True)

Expose a Spark dataframe to the Spark Thrift database

## 3) exposeSparkUdf(sparkUdf, name)

Expose a Spark UDF (User Defined Function) to the Spark Thrift database

## 4) startService(thriftPort=10000, transportMode="binary", httpPath="cliservice", useTruststore=False, truststoreFilePath=".keystore.jks", truststorePassword="datalab")

Start the Thrift service

## 5) stopService()

Stop the Thrift service
