# ModuleLoader

Dynamic Module Loader

Instanciation:
```python
moduleLoader = ModuleLoader()
```
Framework shortcut:
```python
appEnv.getInstance()
```
HowTo: See the demo


## 1) getInstance(moduleName, args=None, className=None, directory=None)

Load an instance of a class dynamically (better than a static approach via "from XXX import YYY")

## 2) loadModule(moduleName, directory=None)

Load a module dynamically (better than a static approach like "from XXX import YYY")

## 3) reloadModule(moduleName, directory=None)

Reload a module dynamically
