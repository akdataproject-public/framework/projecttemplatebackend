# AppEnv

Application environment (context)

Framework object (reference): **appEnv**



## 1) cmdLine(cmd, encoding="UTF-8")

Execute a command line (return both the standard and the error outputs)

## 2) copyFile(srcFile, dstFile)

Copy a file

## 3) createSparkSession()

Create the Spark session based on the Spark config file

## 4) deleteFile(file)

Delete a file

## 5) getEnvVar(name, default=None, environment=None)

Get the value of an environment variable

## 6) getInstance(moduleName, className=None, args=None, directory=None)

Load an instance of a class dynamically (better than a static approach via "from XXX import YYY")

## 7) getSourceCodeInventory(module, includePrivate=False)

Get an inventory of a source code (as a dictionnary)

## 8) getSourceCodePandasInventory(module, includePrivate=False)

Get an inventory of a source code (as a Pandas dataframe)

## 9) getSourceCodeSparkInventory(module, includePrivate=False)

Get an inventory of a source code (as a Spark dataframe)

## 10) getSparkWebUiUrl()

Return the current Spark WebUI URL

## 11) initSpark(spark)

Init the Spark environment based on the Spark config file

## 12) isCodeLoopMode()

Method to call when the developper wants its code looping

## 13) isDirExists(aDir)

Is directory exists

## 14) isFileExists(file)

Is file exists

## 15) isInJupyter()

Check if the current environment is JupyterLab

## 16) isJobAlive(waitInSeconds=0)

Check if the current job is alive

## 17) isSparkConf()

Check if the Spark config file is enabled

## 18) loadModule(moduleName)

Load a module dynamically (better than a static approach like "from XXX import YYY")

## 19) moveFile(srcFile, dstFile)

Move a file

## 20) readFile(file)

Read a local file

## 21) renameFile(srcFile, dstFile)

Rename a file

## 22) setJupyterLabOptions()

Set JupyterLab options if enabled

## 23) splitFilePath(filePath)

Return (parentPath, baseName, fileName, fileExt) of a file path

## 24) writeFile(file, data, appendMode=False)

Write to a local file
