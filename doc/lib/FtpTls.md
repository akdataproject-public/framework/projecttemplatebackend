# FtpTls

FTPS Client

Instanciation:
```python
ftps = FtpTls(user, passwd, host, acct=None, keyfile=None, certfile=None, context=None, timeout=60)
```


## 1) closeSession()

Close the session

## 2) makepasv()

make PASV (overriden function)

## 3) openConnection(host="127.0.0.1", port=990, timeout=60)

Open a single FTPS connection (without login and password)

## 4) openSession(user="anonymous", password=None, host="127.0.0.1", port=990, timeout=60)

Open a new FTPS session with login and password
