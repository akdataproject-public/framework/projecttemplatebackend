# YamlConfigFile

YAML file library

Instanciation:
```python
ycf = YamlConfigFile(yamlConfigFile, environment)
```
Framework object (reference): **cfgYaml**

HowTo: See the demo


## 1) get(key, strip=True, lower=False, upper=False, default=None)

Get the value of a Yaml property (e.g: get("item.price"))
