# KafkaRest

KAFKA REST Client

Framework object (reference): **kfr**

Init properties: **url, auth, proxies, groupName, consumerInstanceId, topic**



## 1) commitOffsets(topicPartitionOffsetList=[{"topic": "myTopic", "partition": 0, "offset": 0}])

READING: Commit a list of offsets for the consumer. When the post body is empty, it commits all the records that have been fetched by the consumer instance

## 2) consume(topic=None, partition=0, offset=None, autoCommit=True, onlyValues=True, returnJson=True)

READING: High level function for consuming Kafka messages

## 3) createConsumerInstance(autoCommit=True)

READING: Create a new consumer instance in a consumer group

## 4) destroyConsumerInstance()

MANAGING: Destroy a consumer instance

## 5) fetchMessages(onlyValues=False, returnJson=False)

READING: Fetch messages for the topics or partitions specified during subscribe

## 6) getConsumerPartitions()

MANAGING: Get the list of partitions currently manually assigned to a consumer

## 7) getLastCommittedOffsets(topicPartitionList=[{"topic": "myTopic", "partition": 0}])

READING: Get the last committed offsets for the given partitions (whether the commit happened by this process or another)

## 8) overridesFetchOffsets(topicPartitionOffsetList=[{"topic": "myTopic", "partition": 0, "offset": 0}])

READING: Overrides the fetch offsets that the consumer will use for the next set of records to fetch

## 9) produce(message, topic=None, partition=None, key=None)

WRITING: High level function for producing one Kafka message

## 10) produceMessage(topic, value, partition=None, key=None)

WRITING: Produce message to a topic

## 11) seekToFirstOffset(topicPartitionList=[{"topic": "myTopic", "partition": "0"}])

READING: Seek to the first offset for each of the given partitions

## 12) subscribeTopics(topics=None)

READING: Subscribe to a given list of topics
