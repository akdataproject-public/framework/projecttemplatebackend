# Ftp

FTP Client

Instanciation:
```python
ftp = Ftp(user, passwd, host, timeout)
```


## 1) closeSession()

Close the session

## 2) openConnection(host="127.0.0.1", port=21, timeout=60)

Open a single FTP connection (without login and password)

## 3) openSession(user='anonymous', password=None, host='127.0.0.1', port=21, timeout=60)

Open a new FTP session with login and password
