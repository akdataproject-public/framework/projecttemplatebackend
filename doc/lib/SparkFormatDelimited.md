# SparkFormatDelimited

Spark library (deprecated) for delimited files (e.g: CSV files)

Instanciation:
```python
sfd = SparkFormatDelimited(spark)
```
Framework shortcut:
```python
sdu.loadCsv() -> fileToDf()
```


## 1) bytesShowFirstLines(dataBytes, nbrLines=20)

Show first lines of a data raw bytes

## 2) bytesToDf(dataBytes, selectedColumns=None)

Load bytes to a Spark dataframe

## 3) fileShowFirstLines(filePath, nbrLines=20, encoding=None)

Show first lines of a data raw file

## 4) fileToDf(filePath, selectedColumns=None, encoding=None)

Load a dirty CSV file to a Spark dataframe

## 5) localBigFileToHdfs(localFilePath, hdfsFilePath)

Moving a local big file to HDFS

## 6) localExcelToDf(localExcelPath, sheetName="sheet1", columnDelimiter=";", removeTempCsvFile=True, selectedColumns=None, encoding=None, quoteAll=False)

Load Excel file to a Spark dataframe

## 7) localFileEncoding(aFile)

Get file encoding

## 8) rddShowFirstLines(rdd, nbrLines=20)

Show first lines of a Spark RDD

## 9) rddToDf(rdd, selectedColumns=None)

Load a Spark RDD to a Spark dataframe

## 10) showDf(percent, columns=None, maxRows=0, maxColumns=0)

Show the first lines of a Spark dataframe graphically
