# StringUtils

String Utilities

Framework object (reference): **txtu**



## 1) removeAccents(text, dropReplacementCharater=False)

Remove accents into a string

## 2) replace(text, stdRules=None, reRules=None, reRulesFirst=False)

Make standard and regularExpression replacements based on key-value pairs

## 3) replaceEnvVars(text, searchPattern=".*?\${(\w+)}.*?", environment=None, nullByEmpty=False)

Replace environment variables (e.g: "${XXX}") into a string
