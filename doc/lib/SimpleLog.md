# SimpleLog

Simple log library (including meta-fields for monitoring tools)

Framework object (reference): **log**

HowTo: See the demo


## 1) getCurrentDatetime(pattern="%Y-%m-%d %H:%M:%S")

Get the current datetime

## 2) log(msg, logLevel=None, metaName=None, metaLevel=None, metaType=None, metaDesc=None)

simple log (without timestamp)

## 3) logError(msg, logLevel=None, metaName=None, metaLevel=None, metaType=None, metaDesc=None)

log error (with timestamp)

## 4) logInfo(msg, logLevel=None, metaName=None, metaLevel=None, metaType=None, metaDesc=None)

log info (with timestamp)

## 5) logWarning(msg, logLevel=None, metaName=None, metaLevel=None, metaType=None, metaDesc=None)

log warning (with timestamp)
