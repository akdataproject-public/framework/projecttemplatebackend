# SparkMLUtils

Spark Machine-Learning Utilities

Framework object (reference): **smu**



## 1) createFeaturesColumn(df, columns=None, featuresColumn="FEATURES")

Create a features column

## 2) getNumericalColumns(df)

Get numerical columns

## 3) normalize(df, columns=None, columnsPrefix=None)

Normalize numerical columns of a dataframe

## 4) show(df, count=None, percent=None, maxColumns=0, index=True)

Show a Spark dataframe in graphic mode

## 5) showHistogram(df, columnName, greaterThan=None, barSize=1)

Show an histogram based on a single column
