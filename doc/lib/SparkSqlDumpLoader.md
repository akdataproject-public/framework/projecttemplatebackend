# SparkSqlDumpLoader

Spark SQL Dump Loader

Instanciation:
```python
sdl = SparkSqlDumpLoader(spark)
```
Framework shortcut:
```python
sdu.loadSqlDump() -> load()
```


## 1) findNumberOfSchemas(sqlDump, dbType="mysql")

Find the number of schemas from the SQL dump file (necessary for "CREATE TABLE")

## 2) getUniqueSchema(sqlDump, dbType="mysql")

Get the schema from the SQL dump file

## 3) load(sqlDump, dbType="mysql", delimiter=', ', setColNames=True, setColTypes=True, inferSchema=False, mergeSchema=False, repartition=32)

Load a SQL dump file into a Spark dataframe
