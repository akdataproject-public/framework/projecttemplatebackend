# SparkDfUtils

Spark Dataframe Utilities

Framework object (reference): **sdu**

HowTo: See the demo


## 1) addConsecutiveIndex(df, indexColumnName)

Add consecutive index into a Spark dataframe (Advice: Sort your data before)

## 2) append(df, data)

Append one or several rows into a Spark dataframe

## 3) arrangePartitions(df, nbrPartitions)

Arrange the partitions number of a Spark dataframe (uses either coalesce or repartition)

## 4) cacheDiskOnly(df)

Cache a Spark dataframe only in disk

## 5) cacheMemoryAndDisk(df)

Cache a Spark dataframe both in memory and disk

## 6) cacheMemoryOnly(df)

Cache a Spark dataframe only in memory

## 7) castColumn(df, columnName, columnType)

Cast a single column into a Spark dataframe

## 8) castColumnToArray(df, columnName, subType='string', delimiter=', ', prepareForNumbers=False)

Cast a single column to an Array<type> into a Spark dataframe

## 9) castColumns(df, columnNames, columnType)

Cast several columns with the same type into a Spark dataframe

## 10) castColumnsToArray(df, columnNames, subType='string', delimiter=', ', prepareForNumbers=False)

Cast several columns to an Array<type> into a Spark dataframe

## 11) columnToArrayOfStrings(df, column, nullValues = ['', "None"])

Tranform a column to an array of strings by replacing bad keywords by null

## 12) convertExcelDateToTimestamp(df, sourceColumn, targetColumn=None)

Create a column into a Spark dataframe by converting an Excel timestamp to a Spark timestamp

## 13) convertRowsAsArrays(rows, columns=None)

Convert Saprk rows as arrays containing values

## 14) convertTimestampToExcelDate(df, sourceColumn, targetColumn=None)

Create a column into a Spark dataframe by converting a Spark timestamp to an Excel timestamp

## 15) copyColumn(df, sourceColumnName, targetColumnName)

Copy a column to a new one into a Spark dataframe

## 16) createColumnWithCurrentDate(df, columnName)

Create a column with the current date

## 17) createColumnWithCurrentTimestamp(df, columnName)

Create a column with the current timestamp

## 18) createColumnWithMilliseconds(df, millisecondsColumn, timestampColumn)



## 19) createColumnWithRule(df, columnName, columnType, ruleClass, ruleName, columns=None)

Create a column into a Spark dataframe by calling a rule from an object class (e.g: business rules)

## 20) createColumnWithValue(df, columnName, columnType=None, value=None)

Create a column with a constant value into a Spark dataframe

## 21) createDataFrame(schema, data=None)

Create a Spark dataframe from list

## 22) createDataFrameFromDictionnary(data)

Create a Spark dataframe from a dictionnary

## 23) createTableFromDf(df, baseName, tableName, cacheTable=False)

Create a SQL table from a Spark dataframe

## 24) delDirectory(dir)

Delete a path and all its contents (works with HDFS and local FS)

## 25) delDirectoryContent(dir)

Delete the contents of a directory, not the directory itself (works with HDFS and local FS)

## 26) dropColumn(df, columnName)

Drop a column into a Spark dataframe

## 27) dropColumns(df, columns)

Drop several columns into a Spark dataframe

## 28) dropDuplicates(df, columns)

Drop duplicate(s) into a Spark dataframe

## 29) dropExistingRowsFromOtherDf(df, dfKey, otherDf, otherDfKey)

Drop rows into a Spark dataframe which exist from another Spark dataframe (by joining primary keys)

## 30) flatten(df, separator='_', flattenArray=False)

Flatten a Spark dataset (an embedded structure) into a Spark dataframe (a tabular structure)

## 31) flushAsParquet(df, path, nbrPartitions=None, partitionNames=None, compression=False, inferSchema=False, mergeSchema=False)

Flush as Parquet (save then reload the data to/from Parquet)

## 32) getAggregate(df, columnName, asColumnName=None, groupByColumns=None, agg='max', where=None)

Get an aggregated dataframe by specifying a column name, asColumn, groupBy, a where clause and an aggregate function

## 33) getAggregatedValue(obj, columnName, agg='max', where=None)

Get aggregation from a Spark dataframe by specifying a column

## 34) getColumnType(df, columnName)

Get the type of a column into a Spark dataframe

## 35) getDirectories(parentPath, recursive=False, startsWith=None, contains=None, endsWith=None)

Get directories from a parent directory (works with HDFS and local FS)

## 36) getDistinctValues(obj, columnName, where=None, forceStringType=True)

Get distinct values from a Spark dataframe by specifying a column

## 37) getDuplicatesCount(df, columns)

Count duplicate(s) into a Spark dataframe

## 38) getFileSize(filePath)

Get the size of a file (works with HDFS and local FS)

## 39) getFiles(parentPath, recursive=False, startsWith=None, contains=None, endsWith=None)

Get the list of files from a parent directory (works with HDFS and local FS)

## 40) getFirst(df, columnName)

Get the column's value of a dataframe's first row

## 41) getFirstRow(df)

Return the first Spark row

## 42) getFirstRowAsArray(df)

Get the first Spark row as array

## 43) getNumberOfPartitions(df)

Get the number of partitions from a Spark dataframe

## 44) getNumericalColumns(df)

Get numerical columns

## 45) getRows(df)

Return the Spark rows

## 46) getRowsAsArrays(df)

Get Spark rows as arrays

## 47) getSchema(df)

Get the schema of a Spark dataframe (as a list)

## 48) getSchemaAsDict(df)



## 49) getSchemaAsJson(df)



## 50) getSchemaAsList(df)



## 51) getSchemaAsTree(df, lightened=False)



## 52) getSchemaTree(df, lightened=False)

Get the schema of a Spark dataframe (as a tree)

## 53) getSparkConf()

Get the Spark configuration

## 54) insertDfIntoTable(df, baseName, tableName, cacheTable=False)

Insert a Spark dataframe into a SQL table

## 55) isEmpty(df, byCounting=False)

Check if a Spark dataframe is empty

## 56) isFileEmpty(file)

Check if a file is empty

## 57) loadAvro(inputDir, inferSchema=False, mergeSchema=True, partitions=None)

Load Avro data source as a Spark dataset or dataframe

## 58) loadCsv(file, header=True, delimiter=None, inferSchema=True, trimColumns=True, dropAccents=False, replaces=None, colDateTime=None, colFileName=None, colFileSize=None, colNbrRows=None)

Load a dirty or well formed CSV data source as a Spark dataframe

## 59) loadExcel(file, sheetName='sheet1', header=True, tmpDelimiter='|', inferSchema=True, trimColumns=True, dropAccents=False, replaces=None, colDateTime=None, colFileName=None, colFileSize=None, colNbrRows=None, quoteAll=True)

Load an Excel data source as a Spark dataframe

## 60) loadJson(obj, inferSchema=False, isFile=True, transform=True)

Load Json data source as a Spark dataset

## 61) loadJsonString(json, inferSchema=False, preTransform=True, stdReplaces=None, reReplaces=None, reReplacesFirst=False)

Load a Json string as a Spark dataset

## 62) loadMongoDb(userLogin, userPassword, server='127.0.0.1', port=27017, database=None, table=None, trustoreFile=None, trustorePassword=None)

Load a MongoDb data source as a Spark dataset

## 63) loadParquet(inputDir, inferSchema=False, mergeSchema=False, partitions=None, dropDuplicates=None)

Load Parquet data source as a Spark dataframe

## 64) loadSimpleCsv(file, header=True, delimiter=';', inferSchema=True)

Load a well formed CSV data source as a Spark dataframe

## 65) loadSqlDump(sqlDump, dbType='mysql', delimiter=', ', setColNames=True, setColTypes=True, inferSchema=False, mergeSchema=False, repartition=32)

Load a SQL dump file as a Spark dataset

## 66) loadSqlQuery(query, driver, url, user, password)



## 67) loadSqlTable(table, driver, url, user, password)



## 68) loadXml(inputFile, outputFile=None, inferSchema=False, returnTuple=False)

Load a XML file from a local FileSystem into a Spark dataset (requires 'xmltodict' dependency)

## 69) movePath(src, dest)

Move a path (works with HDFS and local FS)

## 70) organizeColumns(df, columns, atStart=False, atEnd=False)

Organize the columns of a dataframe

## 71) pandasToSpark(df, forceStringType=True)

Convert a Pandas dataframe to a Spark dataframe

## 72) prepareArrayColumnForNumbers(df, columnName)

Prepare an array column to be parsed as an array of numbers (Array<Integer>, Array<Long>, Array<Float>, Array<Doubles>).

## 73) renameAllColumns(df, dropAccents=False, upperCase=False, lowerCase=False, stdReplaces=[['.', '_']], reReplaces=None, reReplacesFirst=False)

Rename several columns into a Spark dataframe

## 74) renameColumn(df, oldName, newName)

Rename a column into a Spark dataframe

## 75) replace(df, kvPairs=None, columns=None)

Replace key-value pairs into a Spark dataframe, by specifying some columns (or all columns if None)

## 76) replaceByNull(df, values=[''], columns=None)

Replace by Null a list of values into a Spark dataframe, by specifying some columns (or all columns if None)

## 77) replaceNotNullBy(df, value, columns=None)

Replace not Null by a value into a Spark dataframe, by specifying some columns (or all columns if None)

## 78) replaceNotNullByNull(df, columns=None)

Replace not Null by Null into a Spark dataframe, by specifying some columns (or all columns if None)

## 79) replaceNullBy(df, value, columns=None)

Replace Null by a value into a Spark dataframe, by specifying some columns (or all columns if None)

## 80) saveAsCsv(df, filePath, delimiter=';', header=True, quoteAll=False, overwrite=True, nbrPartitions=None)

Save a Spark dataframe as CSV format (with possible several partitions)

## 81) saveAsExcel(df, filePath, sheetName='Sheet1')

Save a Spark dataframe as Excel format

## 82) saveAsJson(df, filePath, overwrite=True, nbrPartitions=None)

Save a Spark dataframe as JSON format (with possible several partitions)

## 83) saveAsParquet(df, filePath, compression=True, overwrite=True, partitionNames=None, nbrPartitions=None)

Save a Spark dataframe a Parquet format

## 84) saveAsSingleCsv(df, csvFile, header=True, delimiter=';')

Save a Spark dataframe as a single CSV file

## 85) saveAsSingleJson(df, file)

Save a Spark dataframe as a single JSON file

## 86) show(df, count=None, percent=None, maxColumns=0, index=True)

Show a Spark dataframe in graphic mode

## 87) showFile(file, nbrOfLines=10)

Show the first line(s) of a raw file whatever its size

## 88) showHistogram(df, columnName, greaterThan=None, barSize=1)

Show an histogram based on a single column

## 89) sort(df, sortedColumnNames=None, ascending=True)

Sort a Spark dataframe by specifying a set of columns

## 90) sparkToPandas(df)

Convert a Spark dataframe to a Pandas dataframe

## 91) uncache(df)

Uncache (or unpersist) a Spark dataframe
