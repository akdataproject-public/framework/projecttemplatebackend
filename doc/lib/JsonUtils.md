# JsonUtils

JSON Utilities

Framework object (reference): **jsu**



## 1) dictToJson(jsonDict)

Converting a list of dictionaries to a Python json

## 2) dictToJsonForFile(jsonDict, lineSeparator='\n')

Converting a list of dictionaries to a json for a file

## 3) jsonForFileToDict(jsonString, lineSeparator='\n')

Converting a json coming from a file to a list of dictionaries

## 4) jsonToDict(jsonString)

Converting a Python json to a list of dictionaries

## 5) toJson(jsonString)

Converting a json coming from a file to a Python json

## 6) toJsonForFile(jsonString, lineSeparator='\n')

Converting a Python json to a json for a file
