# TikaWrapper

Tika Wrapper for Office documents text extracting (e.g: pdf, doc, odt, xlsx, etc.)

Framework object (reference): **tika**

HowTo: See the demo


## 1) extractMetadata(filePath, encoding="UTF-8", returnTuple=False)

Extract metadata from a document

## 2) extractText(filePath, encoding="UTF-8", returnTuple=False, dropSpacesInWords=False)

Extract text from a document
