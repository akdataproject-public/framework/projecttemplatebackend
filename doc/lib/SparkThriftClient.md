# SparkThriftClient

Spark Thrift Client

Framework object (reference): **sthc**

HowTo: See the demo


## 1) cacheTable(table, database=None)

Cache a SQL table

## 2) connect(host, port, user=None, password=None, driverType="JayDeBeApi", jdbcUrlPattern=None, sparkHome=None)

Connection to the database server

## 3) createDatabase(database)

Create a database

## 4) createTableFromCsv(csvFile, table, database=None, cacheTable=False, header=True, delimiter=';', inferSchema=False)

Create a SQL table from a CSV file

## 5) createTableFromDf(df, table, database=None, cacheTable=False)

Create a SQL table from a Spark Dataframe

## 6) createTableFromPandas(df, table, database=None, cacheTable=False)

Create a SQL table from a Pandas Dataframe

## 7) createTableFromParquet(parquetPath, table, database=None, cacheTable=False)

Create a SQL table from Parquet data

## 8) dropDatabase(database)

Drop a database

## 9) dropTable(table, database=None)

Drop a SQL table

## 10) getTable(table, database=None, returnPandas=False)

Get a SQL table

## 11) refreshTable(table, database=None)

Refresh a SQL table

## 12) setTmpDir(tmpDir)

Define the temporary directory

## 13) sql(query, returnPandas=False)

Execute a SQL query

## 14) uncacheTable(table, database=None)

Uncache a SQL table
