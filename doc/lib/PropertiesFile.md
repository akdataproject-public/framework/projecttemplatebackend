# PropertiesFile

Properties file library

Instanciation:
```python
props = PropertiesFile(propertiesFile, environment)
```
Framework object (reference): **cfgProps**

HowTo: See the demo


## 1) get(key)

Get the value of a property

## 2) merge(propertiesFile, environment=None)

Merge the current properties with new properties
