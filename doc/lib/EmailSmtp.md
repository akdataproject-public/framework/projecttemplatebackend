# EmailSmtp

Email SMTP client

Framework object (reference): **smtp**

Init properties: **host, port, user, password, isTls**

HowTo: See the demo


## 1) sendMessage(fromAddr, toAddrs, subject, content, files=None)

Send a email with the possibility to attach one or several  files
