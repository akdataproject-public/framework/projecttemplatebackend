# HiveClient

Hive Client

Instanciation:
```python
hc = HiveClient()
```


## 1) close()

Close the Hive server connection

## 2) connect()

Open the connection to the Hive server

## 3) createDatabase(database)

Create a database

## 4) createTableFromPandas(df, table, database=None, overwrite=True)

Create a Hive table from a Pandas dataframe

## 5) createTableFromSpark(df, table, database=None, overwrite=True)

Create a Hive table from a Spark dataframe

## 6) dropDatabase(database)

Drop a database

## 7) dropTable(table, database=None)

Drop a SQL table

## 8) getDatabases()

Get the list of Hive bases

## 9) getTables(database=None)

Get the list of Hive tables of a base

## 10) loadTableAsPandas(table, database=None, where=None, limit=1000)

Load the content of a Hive table as a Pandas dataframe

## 11) loadTableAsSpark(table, database=None, where=None, limit=1000)

Load the content of a Hive table as a Spark dataframe

## 12) sql(query)

SQL query

## 13) toPandas(df)

Convert a Spark dataframe to a Pandas dataframe

## 14) toSpark(df)

Convert a Pandas dataframe to a Spark dataframe

## 15) useDatabase(database)

Use a specific Hive base
