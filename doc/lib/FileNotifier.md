# FileNotifier

File Notifier

Instanciation:
```python
fn = FileNotifier(listenDir, eventTypes)
```


## 1) getEventTypes()

Get the list of events to catch

## 2) setEventCallback(callbackObject, callbackName)

Set the instance class and the callback method in order to catch events

## 3) setEventTypes(eventTypes=None)

Set the list of events to catch

## 4) setLogCallback(callbackObject, callbackName)

Set the instance class and the callback method in order to catch logs
