# FtpUtils

FTP Utilities

Framework object (reference): **ftpu**

Init properties: **user, password, host, port, secured, timeout**

HowTo: See the demo


## 1) bytesToFile(fileBytes, ftpFilePath)

Upload bytes in memory to a remote file

## 2) bytesToLocalFile(dataBytes, localFilePath)

Convert bytes to a local file

## 3) close()

Close session

## 4) connect()

Common connection

## 5) connectFTP(user="anonymous", password=None, host="127.0.0.1", port=21, timeout=60)

Non secure FTP connection

## 6) connectFTPS(user="anonymous", password=None, host="127.0.0.1", port=990, timeout=60)

Secure FTPS connection

## 7) deleteFile(ftpFilePath)

Delete a remote file

## 8) fileToBytes(ftpFilePath)

Load a remote file as bytes

## 9) fileToLocalFile(ftpFilePath, localFilePath)

Import a remote file to the local file system

## 10) fileToZip(ftpFilePath)

Load a remote file as zipped format

## 11) getFileDatetime(ftpFilePath)

Get the datetime of a remote file

## 12) getFileSize(ftpFilePath)

Get the size of a remote file

## 13) localFileToBytes(localFilePath)

Convert a local file to bytes

## 14) localFileToFile(localFilePath, ftpFilePath)

Upload a local file to a remote file

## 15) moveFile(sourceFtpFilePath, targetFtpFilePath)

Move a remote file

## 16) renameFile(sourceFtpFilePath, targetFtpFilePath)

Rename a remote file

## 17) scanFiles(ftpParentFolder="/", recursive=False, startsWith=None, contains=None, endsWith=None)

Scan remote files

## 18) scanFolders(ftpParentFolder="/", recursive=False, startsWith=None, contains=None, endsWith=None)

Scan remote folders
