# [PythonProject Template] - **"Let's Develop and Run !"**

**[PythonProject Template]** consists to a framework made for people who want to develop projects with Python (and PySpark) by using Agile and DevOps approaches.

It allows to start projects without worrying about the underlying deployment mecanisms and execution infrastructures.

It also provides many useful libraries that make life easier for developers.

Finally it supports JupyterLab developments by automatically converting every notebook into Python code for deployment.

In one word with [PythonProject Template], **"Let's Develop and Run !"**.

## 1 - How to import [PythonProject Template]

To start a new project, import [PythonProject Template] like this:
```shell
git clone https://gitlab.com/akdataproject/socle/template/pythonproject.git && rm -rf pythonproject/.git
```

- Note if you are behind a proxy server:

Before importing your new project you may need to configure **git** like this:
```shell
git config --global http.proxy http://proxyUsername:proxyPassword@proxyHost:proxyPort
```

## 2 - Rename your new project

Let's say you want to name your new project **MyProject**, so rename it like this:
```shell
mv pythonproject MyProject
```

## 3 - Project and job definitions

A **project** contains one or several **jobs**, an each job can be either a notebook or a pure Python code.

### Snippet at the begining of each job
When you create a new job you have to insert at the begining the following ![init code](./lib/template/InitApp.py).

From the init code, don't forget to set the variable **JOB_NAME** (by replacing the "**Default**" value) with exactly the name of your notebook file or your pure Python code.

### Snippet at the end of each job

If you plan to loop your job you have to insert at the end the following ![loop code](./lib/template/EndApp.py), 
then replace the function **aJobImplementation()** by your own function you want to loop.

## 4 - Developing with Notebooks

All the notebooks must be under the directory **./notebook**.

From the following ![examples](./notebook), choose a notebook then **"Let's Develop and Run !"**

If you copy a notebook from an example above, don't forget at the begining to set the variable **JOB_NAME** with exactly the name of your new notebook.

## 5 - Developing directly with Python

If you want to deploy and run a pure Python code as a job, then the source code must be moved under the directory **./src**.

Now **"Let's Develop and Run !"**

From the next section, see how to deploy and run jobs.

## 6 - How to deploy and run your jobs in production

### Deploy
**To deploy a notebook** as a job, call the following command:
```shell
./bin/deploy.sh ./src/MyJob.ipynb
```
The command above will convert the notebook **./src/MyJob.ipynb** to the Python job **./job/MyJob.py**.

**To deploy a pure source code** as a job, call the following command:
```shell
./bin/deploy.sh ./src/MyJob.py
```
The command above will copy the source code **./src/MyJob.py** to the Python job **./job/MyJob.py**.

### Start/Stop

The **start** command runs jobs which are deployed in the directory **./job**.

To execute a job just once in console mode, call the command:
```shell
./bin/start.sh MyJob console once
```

To execute a job in console mode (once or in loop mode), call the command:
```shell
./bin/start.sh MyJob console
```

To execute a job in background mode, call the command:
```shell
./bin/start.sh MyJob background
or
./bin/start.sh MyJob
```

The **stop** command stop jobs which are running.

To stop a job, call the command:
```shell
./bin/stop.sh MyJob
```

### Logs
To check in realtime the logs of a job, call the command:
```shell
# Check the logs
./bin/logs.sh MyJob

# Check the logs by starting from the last 100 lines
./bin/logs.sh MyJob 100
```

## 7 - How to loop a job

The loop modes are: SHELL, CODE and NOP (if you don't want to loop your application).

SHELL mode enable loop managed by the shell script.

CODE mode enable loop managed by the code.

NOP mode means no loop.

Edit the config file **./conf/start.conf** and modify the following variables:
```
LOOP_MODE="SHELL"
APP_SLEEP="1"
```
The example above will enable loop managed by the shell script, it will wait for 1s between every loop.

Note: When CODE mode is set, APP_SLEEP must contains float type.

## 8 - Configuring your environment

Edit the config file **./conf/env.conf** an set the two variables **JAVA_HOME** and **ANACONDA_HOME**.

## 9 - Configuring and disable/enable PySpark

To disable PySpark, rename the file **./conf/spark.conf** to **./conf/spark.conf.template**.

To enable PySpark, rename the file **./conf/spark.conf.template** to **./conf/spark.conf**.

To configure the Spark runtime, edit the config file **./conf/spark.conf**.

  - Developer: Philippe ROSSIGNOL (2020-06-11)
